---
project: "{{ index (split .File.Dir "/") 1 }}"
version: "0.0"
patch: "0"
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
type: "post"
draft: true
tags: []
---